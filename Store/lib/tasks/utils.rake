namespace :utils do
  desc "Populate the tables needed to start the Manuel's Store"
  task setup_database: :environment do
    10.times do |i|
      name = Faker::Name.name
      Buyer.create(name: name)
    end

    20.times do |i|
      name = Faker::Commerce.product_name
      description = Faker::Lorem.sentence(3, true, 4)
      stock = Faker::Number.between(10, 50)
      price = Faker::Commerce.price
      Product.create(name: name, description: description, stock: stock, price: price)
    end

    User.create(name:'Manuel', password:'123456', email:'manuel@loja.com.br')
    puts 'Successfully created users, buyers and products'
  end
end
