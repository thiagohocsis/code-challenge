module ApplicationHelper
  def configure_date(date)
    Date.strptime(date, "%d-%m-%Y").strftime("%d/%m/%Y")
  end
end