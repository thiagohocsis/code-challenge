class Api::V1::ItemsController < ApplicationController
  before_action :set_item, only: [:destroy]
  def create
    @item = Item.new(item_params)
    if @item.save
      render json: @item, status: :created
    else
      render json: @item.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @item.destroy
  end

  private

  def item_params
    params.require(:item).permit(:quantity, :sale_price, :product_id, :order_id)
  end

  def set_item
    @item = Item.find(params[:id])
  end
end