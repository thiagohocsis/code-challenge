class Api::V1::BuyersController < ApplicationController
  before_action :set_buyer only: [:destroy]

  def create
    @buyer = Buyer.new(buyer_params)
    if @buyer.save
      render json: @buyer, status: :created
    else
      render json: @buyer.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @buyer.destroy
  end

  private

  def buyer_params
    params.require(:buyer).permit(:name)
  end

  def set_buyer
    @buyer = Buyer.find(params[:id])
  end
end