class Api::V1::OrdersController < ApplicationController
  before_action :set_order, only: [:show, :update, :destroy]
  def index
    @orders = Order.all
  end

  def show; end

  def create
    @order = Order.new(order_params)
    if @order.save
    else
      render json: { errors: @order.errors }, status: 422
    end
  end

  def update
    if @order.update(order_params)
      render json: @order
    else
      render json: @order.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @order.destroy
  end

  def average_ticket
    @start_date = configure_date(params[:start_date])
    @end_date =  configure_date(params[:end_date])
    orders = Order.created_between(@start_date, @end_date)
    @average_ticket = (orders.map(&:total).sum / orders.count).round(2)
  end

  private

  def order_params
    params.require(:order).permit(:purchase_date, :shipping_price, :status, :buyer_id, items_attributes: [
        :id, :product_id, :quantity, :sale_price])
  end

  def set_order
   @order = Order.find(params[:id])
  end

end