json.array! @orders do |order|
  json.id order.id
  json.purcharse_date order.purchase_date
  json.status order.status
  json.shipping_price order.shipping_price
  json.buyer do
    json.name order.buyer.name
  end
    json.items order.items  do |item|
      json.product_id item.product_id
      json.quantity item.quantity
      json.sale_price item.sale_price
  end
end