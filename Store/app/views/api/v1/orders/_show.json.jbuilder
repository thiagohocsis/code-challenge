json.id @order.id
json.purcharse_date @order.purchase_date
json.status @order.status
json.shipping_price  "R$ #{@order.shipping_price}"
json.total  "R$ #{@order.total}"
json.buyer do
  json.name @order.buyer.name
end
json.items @order.items  do |item|
  json.product_id item.product_id
  json.quantity item.quantity
  json.sale_price "R$ #{item.sale_price}"
end