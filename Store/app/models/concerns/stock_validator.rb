class StockValidator < ActiveModel::Validator
  def validate(record)
    record.items.each do |item|
      if !Product.where(id: item.product_id).any?
        record.errors.add(:product, 'Product invalid')
      else
        if item.quantity > item.product.stock
          record.errors.add(:stock, 'Not enough stock')
        end
      end
    end
  end
end