class Item < ApplicationRecord
  belongs_to :product
  belongs_to :order
  before_create :set_price!

  def set_price!
    self.sale_price = self.product.price
  end

end
