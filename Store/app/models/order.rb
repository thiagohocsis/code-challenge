class Order < ApplicationRecord
  belongs_to :buyer
  has_many :items, dependent: :destroy
  accepts_nested_attributes_for :items, allow_destroy: true
  validates_with StockValidator
  after_create :set_total!


  enum status: [:new_order,  :approved, :delivered, :canceled]

  def decrement_stock
    self.items.each do |item|
      total = Product.where(id: item.product_id).first.stock - item.quantity
      new_stock = total
      item.product.update_attribute(:stock, new_stock)
    end
  end

  def set_total!
    sum_total = self.items.map{|item| item.sale_price * item.quantity}.sum.round(2)
    self.update_attribute(:total, sum_total)
    decrement_stock
  end


  scope :created_between,
        lambda {|start_date, end_date|
                 where("orders.purchase_date  >= ? AND orders.purchase_date <= ?",
                 start_date.to_date.strftime("%d/%m/%Y"),
                       end_date.to_date.strftime("%d/%m/%Y") )
        }


end
