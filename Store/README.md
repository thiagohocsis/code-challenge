#Desafio - SkyHub

### API para a loja do "seu" Manuel

* Ruby versão - 2.4.1

* Rails versão 5.0.7.2

* Configuração - ```bundle install```

* Criação do DB - ```rails db:create db:migrate```

* Inicialização do DB - ```rake utils:setup_database```

* Como rodar a suite de teste - Acesse o projeto pelo terminal e digite ```rspec```

* Como rodar a ferramenta para code smells - Acesso o projeto pelo terminal e digite ```rubocop```

* Ferramenta utilizada para realização dos requests na API - POSTMAN, disponivel para downlaod em
https://www.getpostman.com/


### Considerações sobre o projeto.

O sistema é composto pelas entidades, Buyer, Item, Order, Product e User, ao ler as instruções
contidas no README fornecido, tive uma idéia diferente da descrita para criar os Pedidos, um Item só é criado quando
um pedido é feito, com uma relação Pedido tem muitos Items com cardinalidade 1..N, e um Item pertence ao Pedido e também ao Produto, sendo
assim quando um Pedido é feito, ele possui um Item e dentro desse item temos o id do Produto que ele pertence. Para isso foi utilizado 
um recurso do framework Ruby on Rails chamado nested_attributes.

* Um JSON de exemplo utilizando o método HTTP POST no endpoint '/api/v1/orders':

```javascript
{
   "order":{
      "purchase_date":"12/11/2019",
      "buyer_id":1,
      "shipping_price":110,
      "status":"delivered",
      "items_attributes":[
         {
            "product_id":1,
            "quantity":1
         },
          {
            "product_id":1,
            "quantity":1
         }
      ]
   }
}
```

e como retorno teriamos o seguinte JSON.

```javascript
{
    "id": 1,
    "purcharse_date": "12/11/2019",
    "status": "delivered",
    "shipping_price": "R$ 110.0",
    "total": "R$ 91.27",
    "buyer": {
        "name": "Jorge Wisoky IV"
    },
    "items": [
        {
            "product_id": 1,
            "quantity": 1,
            "sale_price": "R$ 89.87"
        },
        {
            "product_id": 2,
            "quantity": 1,
            "sale_price": "R$ 1.4"
        }
    ]
}
```

* Para verificar todas os Pedidos presentes no Sistema utilzamos o Endpoint "api/v1/orders" com o método GET.
```javascript
[
    {
        "id": 1,
        "purcharse_date": "01/01/2019",
        "status": "delivered",
        "shipping_price": 110,
        "buyer": {
            "name": "Devon Sauer"
        },
        "items": [
            {
                "product_id": 1,
                "quantity": 2,
                "sale_price": 44.94
            },
            {
                "product_id": 3,
                "quantity": 3,
                "sale_price": 84.91
            }
        ]
    },
    {
        "id": 2,
        "purcharse_date": "15/01/2019",
        "status": "delivered",
        "shipping_price": 110,
        "buyer": {
            "name": "Devon Sauer"
        },
        "items": [
            {
                "product_id": 1,
                "quantity": 2,
                "sale_price": 44.94
            },
            {
                "product_id": 3,
                "quantity": 3,
                "sale_price": 84.91
            }
        ]
    },
    {
        "id": 3,
        "purcharse_date": "25/01/2019",
        "status": "delivered",
        "shipping_price": 110,
        "buyer": {
            "name": "Devon Sauer"
        },
        "items": [
            {
                "product_id": 1,
                "quantity": 2,
                "sale_price": 44.94
            },
            {
                "product_id": 3,
                "quantity": 3,
                "sale_price": 84.91
            }
        ]
    }
]
```

* Para verificar os dados de um determinado pedido o endpoint é GET 'api/v1/orders/:id', no exemplo o Pedido com id = 1.
```javascript
{
    "id": 1,
    "purcharse_date": "01/01/2019",
    "status": "delivered",
    "shipping_price": "R$ 110.0",
    "total": "R$ 344.61",
    "buyer": {
        "name": "Devon Sauer"
    },
    "items": [
        {
            "product_id": 1,
            "quantity": 2,
            "sale_price": "R$ 44.94"
        },
        {
            "product_id": 3,
            "quantity": 3,
            "sale_price": "R$ 84.91"
        }
    ]
}

```

* Para atualizar um pedido o endpoint é PUT 'api/v1/orders/:id', no exemplo vai ser alterado o status do pedido com id de delivered para canceled.

```javascript
{
    "id": 1,
    "purchase_date": "25/01/2019",
    "shipping_price": 110,
    "status": "canceled",
    "buyer_id": 1,
    "total": 344.61,
    "created_at": "2019-05-02T05:49:56.129Z",
    "updated_at": "2019-05-02T05:55:06.512Z"
}
```

* Para calcular o Ticket Médio é utilizando o endpoint GET 'orders/average_ticket/:start_date/:end_date', onde start_date é data inicial
e end_date é a data final para o calculo do Ticket Médio. Um exemplo de requisição '/api/v1/orders/average_ticket/01-01-2019/25-01-2019'
e seu retorno.

```javascript
{
    "start_date": "01/01/2019",
    "end_date": "25/01/2019",
    "average_ticket": "R$ 344.61"
}
```

* Para deletar um Pedido o endpoint é DELETE 'api/v1/orders/:id' e como retorno obtemos o status 204 no content.

* Quando um pedido é realizado e o produto não tem estoque suficiente temos como retorno um JSON:

```javascript
{
    "errors": {
        "stock": [
            "Not enough stock"
        ]
    }
}
```

* Toda vez que um pedido é realizado com sucesso o estoque de produto é decrementado para isso é utilizando no Modelo Order
o método decrement_stock

* O total de um pedido é sempre calculado multiplicando o valor dos seus Items pelos respectivos preços.

* Quando um Pedido é feito contendo um Produto que não existe é retornado um JSON.
```javascript
{
    "errors": {
        "product": [
            "Product invalid"
        ]
    }
}
```
* Para verificação da cobertura de testes foi utilizada a gem Simplecov e até a entrega desse desafio o coverage era de 91.94%.

* Com o intuito de criar um sample data foi criada um rake task chamada utils:setup_database e nela são criados os Produtos, Compradores e o
dono da loja o "seu" Manuel.
