Rails.application.routes.draw do
  devise_for :users

  namespace :api do
    namespace :v1 do
      resources :products
    end
  end

  namespace :api do
    namespace :v1 do
      resources :items
    end
  end

  namespace :api do
    namespace :v1 do
      resources :orders
      get 'orders/average_ticket/:start_date/:end_date', to: 'orders#average_ticket'
    end
  end
end
