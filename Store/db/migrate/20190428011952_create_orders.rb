class CreateOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :orders do |t|
      t.string :purchase_date
      t.references :buyer, foreign_key: true
      t.float :shipping_price
      t.float :total
      t.integer :status, default: 0

      t.timestamps
    end
  end
end
