FactoryBot.define do
  factory :item do
    quantity {3}
    association :product
  end
end