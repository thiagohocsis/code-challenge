FactoryBot.define do
  factory :product do
    name {"Test Product"}
    description {"This is a test Product"}
    price {29.90}
    stock {10}
  end
end