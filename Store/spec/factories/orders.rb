FactoryBot.define do
  factory :order do
    association :buyer
    purchase_date {"01/01/2019"}
    shipping_price {29.90}
    after(:create) do |order, item|
      create_list(:item, 5, order: order)
    end
  end
end