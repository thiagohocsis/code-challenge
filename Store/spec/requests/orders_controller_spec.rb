require 'rails_helper'

RSpec.describe 'OrdersController', type: :request do

  let(:valid_params) do
    {
        "order":{
            "purchase_date":"12/11/2019",
            "buyer_id":1,
            "shipping_price":110,
            "status":"delivered",
            "items_attributes":[
                {
                    "product_id":1,
                    "quantity":1
                },
                {
                    "product_id":1,
                    "quantity":1
                }
            ]
        }
    }
  end

  let(:order_created_json) do
    {
        "id": 1,
        "purcharse_date": "12/11/2019",
        "status": "delivered",
        "shipping_price": "R$ 110.0",
        "total": "R$ 65.54",
        "buyer": {
            "name": "Thanos"
        },
        "items": [
            {
                "product_id": 1,
                "quantity": 1,
                "sale_price": "R$ 32.77"
            },
            {
                "product_id": 1,
                "quantity": 1,
                "sale_price": "R$ 32.77"
            }
        ]
    }.to_json
  end
  describe 'GET api/v1/orders' do
    let!(:order)  {create(:order)}

    it 'will retrieve response code 200' do
      get '/api/v1/orders.json'
      expect(response).to have_http_status(200)
    end

    it 'will retrieve all orders' do
      get '/api/v1/orders.json'
      response_orders = response_body
      expect(response_orders.size).to eq(1)
    end

    it 'must contain attribute values ​​equal to that of orders' do
      get '/api/v1/orders.json'
      response_orders = response_body
      expect(response_orders.first['status']).to eq order.status
      expect(response_orders.first['shipping_price']).to eq order.shipping_price
      expect(response_orders.first['purcharse_date']).to eq order.purchase_date
      expect(response_orders.first['buyer']['name']).to eq order.buyer.name
    end
  end

  describe 'GET api/v1/orders/:id' do
    let!(:order)  {create(:order)}

    it 'will retrieve response code 200' do
      get '/api/v1/orders/1.json'
      expect(response).to have_http_status(200)
    end

    it 'will retrieve order with id = 1' do
      get '/api/v1/orders/1.json'
      response_order = response_body
      expect(response_order['id']).to eq(1)
    end

    it 'should return the same values ​​as the order' do
      get '/api/v1/orders/1.json'
      response_order = response_body
      expect(response_order['status']).to eq order.status
      expect(response_order['shipping_price']).to eq "R$ #{order.shipping_price}"
      expect(response_order['purcharse_date']).to eq order.purchase_date
      expect(response_order['buyer']['name']).to eq order.buyer.name
    end

    it 'will retrieve response code 404' do
      get '/api/v1/orders/3.json'
      expect(response.code).to eq('404')
    end
  end

  describe 'POST api/v1/orders.json' do
    context 'with valid parameters' do
      let!(:buyer)  {create(:buyer)}
      let!(:product)  {create(:product, price:32.77)}
      it 'creates a new order' do
        expect { post '/api/v1/orders.json', params: valid_params }.to change(Order, :count).by(+1)
        expect(response).to have_http_status(200)
      end

      it 'creates a order with the correct attributes' do
        post '/api/v1/orders.json', params: valid_params
        expect(response.body).to eq order_created_json
      end
    end
  end


  describe 'PUT api/v1/orders/:id' do
    let!(:order)  {create(:order)}
    context 'updated with valid params' do
      it 'should be 200 if order updated0' do
        put '/api/v1/orders/1.json', params: valid_params
        expect(response).to have_http_status(200)
      end
    end
  end

  describe 'DELETE api/v1/orders/:id' do
    let!(:order)  {create(:order)}
    context 'Delete order' do
      it 'should be 200 if order updated0' do
        delete '/api/v1/orders/1.json'
        expect(response).to have_http_status(204)
      end
    end
  end

  describe 'test average ticket route' do
      it 'should correctly return the average ticket between the given dates' do
        create(:product, price: 24.50, stock:200)
        items = []
        5.times do |i|
          items << create(:item, quantity: i , product_id: 1)
        end
        5.times do |i|
          create(:order, purchase_date:"0#{i}/01/2019", items: items)
        end
        get '/api/v1/orders/average_ticket/01-01-2019/06-01-2019.json'
        expect(response).to have_http_status(200)
        expect(response_body['start_date']).to eq('01/01/2019')
        expect(response_body['end_date']).to eq('06/01/2019')
        expect(response_body['average_ticket']).to eq('R$ 245.0')
      end
  end

  private

  def response_body
    JSON.parse(response.body)
  end
end