require 'rails_helper'

RSpec.describe 'ProductsController', type: :request do


  describe 'GET api/v1/products' do
    let!(:product)  {create(:product)}

    it 'will retrieve response code 200' do
      get '/api/v1/products.json'
      expect(response).to have_http_status(200)
    end

    it 'will retrieve all records and there is only one ' do
      get '/api/v1/products.json'
      response_products = response_body
      expect(response_products.size).to be(1)
    end

    it 'must contain attribute values ​​equal to that of product' do
      get '/api/v1/products.json'
      response_products = response_body
      expect(response_products.first["name"]).to eq product.name
      expect(response_products.first["description"]).to eq product.description
      expect(response_products.first["price"]).to eq product.price
      expect(response_products.first["stock"]).to eq product.stock
    end
  end

  private

  def response_body
    JSON.parse(response.body)
  end
end
